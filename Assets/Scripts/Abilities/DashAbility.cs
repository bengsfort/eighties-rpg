﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MattBengston;

namespace MattBengston
{
	[RequireComponent (typeof (Rigidbody2D))]
	public class DashAbility : Ability {

		public float duration;
		public float speed;

		bool beingUsed = false;

		Vector2 targetDirection;
		float timeStarted;

		Rigidbody2D rb;


		void Start ()
		{
			rb = gameObject.GetComponent<Rigidbody2D> ();
		}

		protected override bool UsageRequirements ()
		{
			return PlayerManager.instance.Movement.IsControllable ()
				&& (Input.GetAxis ("Horizontal") != 0
					|| Input.GetAxis ("Vertical") != 0)
				&& Input.GetButtonDown ("Jump");
		}

		protected override void UseAbility ()
		{
			beingUsed = true;
			PlayerManager.instance.Movement.DisableControl ();
			targetDirection = new Vector2 (
				Input.GetAxis ("Horizontal"),
				Input.GetAxis ("Vertical") 
			);
			timeStarted = Time.fixedTime;
		}

		void EndAbility ()
		{
			beingUsed = false;
			StartCooldown ();
			PlayerManager.instance.Movement.MakeControllable ();
		}

		void FixedUpdate ()
		{
			if (beingUsed) {
				if (timeStarted + duration > Time.fixedTime) {
					rb.velocity = new Vector2 (
						targetDirection.x * speed,
						targetDirection.y * speed
					);
				} else {
					EndAbility ();
				}
			}
		}
	}
}