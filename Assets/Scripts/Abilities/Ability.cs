﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MattBengston;

namespace MattBengston
{
	public class Ability : MonoBehaviour {

		public string Title = "Ability Title";
		public string ID = "ability";
		//		public Sprite Icon;
		public float CooldownLength = 0f;

		private bool onCooldown;
		private float lastUse;

		protected virtual void UseAbility ()
		{
			Debug.Log ("Ability: " + Title + " was used!");
		}

		protected virtual bool UsageRequirements ()
		{
			Debug.Log ("Ability: " + Title + " usage requirements registered.");
			return false;
		}

		protected void StartCooldown ()
		{
			lastUse = Time.fixedTime;
			onCooldown = true;
		}

		void Update ()
		{
			if (onCooldown) {
				onCooldown = lastUse + CooldownLength > Time.fixedTime;
			}

			if (UsageRequirements () && !onCooldown) {
				UseAbility ();
			}
		}
	}
}