﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MattBengston;

namespace MattBengston
{
	public class DialogManager : MonoBehaviour {

		List<Dialog> dialogs = new List<Dialog> ();

		public void QueueDialog (Dialog dialog)
		{
			dialogs.Add (dialog);
		}

		public void UnqueueDialog (Dialog dialog)
		{
			dialogs.Remove (dialog);
		}

		// @TODO: IEnumerator / Coroutine Dialog system :D 
		// http://bit.ly/2ig8gcH
	}
}