﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MattBengston;

namespace MattBengston
{
	public class UIManager : MonoBehaviour {
		public static UIManager instance;
		public DialogManager DialogMgr;

		void Awake ()
		{
			instance = this;
			if (DialogMgr == null) {
				DialogMgr = new DialogManager ();
			}
		}

		// @TODO: IEnumerator / Coroutine Dialog system :D 
		// http://bit.ly/2ig8gcH
	}
}