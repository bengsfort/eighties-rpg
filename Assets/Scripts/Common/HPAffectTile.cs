﻿using UnityEngine;

namespace MattBengston
{
	public enum HPAffect {
		Damage,
		Heal
	}

	public class HPAffectTile : MonoBehaviour {
		
		public HPAffect hpAffect;
		public float affectAmount;
		public float timePerTick;
		bool playerInRange;
		float lastTick;
		PlayerManager Player;

		

		void AffectPlayer ()
		{
			if (hpAffect == HPAffect.Damage
				&& Player.Health.currentHealth > 0)
				Player.Damage (affectAmount);
			
			if (hpAffect == HPAffect.Heal
				&& Player.Health.currentHealth < Player.Health.baseHealth)
				Player.Heal (affectAmount);
			lastTick = Time.fixedTime;
		}

		void FixedUpdate ()
		{
			if (playerInRange && (Time.fixedTime > lastTick + timePerTick)) {
				AffectPlayer ();
			}
		}

		void OnTriggerEnter2D (Collider2D other)
		{
			if (other.gameObject.CompareTag ("Player")) {
				if (Player == null) Player = PlayerManager.instance;
				AffectPlayer();
				playerInRange = true;
			}
		}

		void OnTriggerExit2D (Collider2D other)
		{
			if (other.gameObject.CompareTag ("Player")) {
				playerInRange = false;
			}
		}
	}
}