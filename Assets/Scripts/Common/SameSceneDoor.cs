﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MattBengston;

namespace MattBengston
{
	[RequireComponent (typeof (SpriteRenderer))]
	[RequireComponent (typeof (BoxCollider2D))]
	public class SameSceneDoor : MonoBehaviour {

		public Transform target;

		public bool locked = false;
		private bool canEnter = false;

		void Start ()
		{
			if (target == null)
				Debug.LogError ("A SameSceneDoor is missing a target!");
		}

		void Update ()
		{
			if (canEnter && Input.GetButtonDown ("Submit")) {
				PlayerManager.instance.Movement.TeleportPlayer (
					new Vector3 (target.position.x, target.position.y, 0f)
				);
				canEnter = false;
			}
		}

		void OnTriggerEnter2D (Collider2D other)
		{
			canEnter = other.gameObject.CompareTag ("Player");
		}

		void OnTriggerExit2D (Collider2D other)
		{
			canEnter = false;
		}
	}
}