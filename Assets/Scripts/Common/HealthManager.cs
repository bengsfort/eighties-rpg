﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MattBengston;

namespace MattBengston
{
	public class HealthManager : MonoBehaviour {

		public float baseHealth = 100f;
		public float currentHealth {
			get; private set;
		}

		// Use this for initialization
		void Start ()
		{
			currentHealth = baseHealth;
		}

		public void Damage (float val)
		{
			Debug.Log ("Player has been damaged by " + val);
			currentHealth = Mathf.Clamp (currentHealth - val, 0f, baseHealth);
			Debug.Log ("New Health: " + currentHealth);
		}

		public void Heal (float val)
		{
			Debug.Log ("Player has been healed by " + val);
			currentHealth = Mathf.Clamp (currentHealth + val, 0f, baseHealth);
			Debug.Log ("New Health: " + currentHealth);
		}

		public float GetHealthValue ()
		{
			return currentHealth;
		}

		public float GetHealthPercentage ()
		{
			return currentHealth / baseHealth;
		}
	}
}