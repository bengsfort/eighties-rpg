﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MattBengston;

namespace MattBengston
{
	public enum PlayerState {
		Dead,
		Alive
	}

	[RequireComponent (typeof (CircleCollider2D))]
	[RequireComponent (typeof (HealthManager))]
	[RequireComponent (typeof (PlayerMovement))]
	public class PlayerManager : MonoBehaviour {

		public static PlayerManager instance;

		public PlayerMovement Movement;
		public HealthManager Health;

		public PlayerState state = PlayerState.Alive;

		void Awake ()
		{
			if (instance == null) {
				instance = this;
			}
		}

		public void Damage (float val)
		{
			Health.Damage (val);

			// Is the player dead?
			if (Health.GetHealthValue () == 0) {
				state = PlayerState.Dead;
				Debug.Log ("Player is dead");
			}
		}

		public void Heal (float val)
		{
			Health.Heal (val);
		}

		void OnGUI ()
		{
			if (GameManager.instance.showDevUI) {
				DevUI ();
			}
		}

		void DevUI ()
		{
			DevHealthUI ();
		}

		void DevHealthUI ()
		{
			Vector2 baseMargin = new Vector2 (10, 10);
			Vector2 baseSize = new Vector2 (150, 35);

			if (GUI.Button (new Rect (baseMargin, baseSize), "Damage Player by 15")) {
				Damage (15f);
			}
			if (GUI.Button (new Rect (baseMargin + new Vector2 (0f, 40), baseSize), "Heal Player by 15")) {
				Heal (15f);
			}

			GUI.Label (new Rect (
				baseMargin + new Vector2(0f, 80),
				new Vector2 (150, 35)),
				"HP: " + (Health.GetHealthPercentage () * 100) + "%" +
				" (" + Health.GetHealthValue () + "/" + Health.baseHealth + ")"
			);
		}
	}
}