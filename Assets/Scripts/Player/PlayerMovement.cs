﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MattBengston
{
	[RequireComponent (typeof (Rigidbody2D))]
	public class PlayerMovement : MonoBehaviour {

		public float moveSpeed = 3f;
		public Vector3 lockedCameraOffset = new Vector3(1f, 1f, 0f);

		public Rigidbody2D rb;
		private Camera mainCamera;

		private bool isControllable = false;
		private bool lockCamera = false;

		// Use this for initialization
		void Start ()
		{
			if (rb == null) {
				rb = gameObject.GetComponent<Rigidbody2D> ();
			}
			MakeControllable ();
		}

		void FixedUpdate ()
		{
			if (isControllable)
				MovePlayer ();
			if (lockCamera)
				MoveCamera ();
		}

		public void MakeControllable (bool followCamera = true)
		{
			isControllable = true;
			lockCamera = followCamera;
			if (lockCamera) {
				mainCamera = Camera.main;
			}
		}

		public void DisableControl ()
		{
			isControllable = false;
		}

		void MovePlayer ()
		{
			float xMovement = Input.GetAxis ("Horizontal");
			float yMovement = Input.GetAxis ("Vertical");
			Vector2 clamped = Vector2.ClampMagnitude (new Vector2(xMovement, yMovement), 1f);
			rb.velocity = clamped * moveSpeed;
		}

		void MoveCamera ()
		{
			Vector3 target = Vector3.MoveTowards (
				mainCamera.transform.position,
				transform.position,
				1f
			);
			mainCamera.transform.position = new Vector3 (
				target.x,
				target.y,
				mainCamera.transform.position.z
			);
		}

		public void TeleportCamera ()
		{
			mainCamera.transform.position = new Vector3 (
				transform.position.x,
				transform.position.y,
				mainCamera.transform.position.z
			);
		}

		public void TeleportPlayer (Vector3 target)
		{
			DisableControl ();
			transform.position = target;
			TeleportCamera ();
			MakeControllable ();
		}

		public bool IsControllable ()
		{
			return isControllable;
		}
	}
}