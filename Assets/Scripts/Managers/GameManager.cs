﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MattBengston;

namespace MattBengston
{
	public class GameManager : MonoBehaviour {

		public static GameManager instance;

		public bool showDevUI;

		private string gameDataPath;

		void Awake ()
		{
			if (instance == null) {
				DontDestroyOnLoad (gameObject);
				instance = this;
			} else {
				Destroy (gameObject);
			}
		}

		// Use this for initialization
		void Start ()
		{
			gameDataPath = Application.persistentDataPath + "/gameDat.a";
			Debug.Log ("Game Data Path set! " + gameDataPath);
		}
	}
}
