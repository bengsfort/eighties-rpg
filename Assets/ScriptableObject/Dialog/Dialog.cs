using UnityEngine;
using MattBengston;

namespace MattBengston
{
    // @TODO: IEnumerator / Coroutine Dialog system :D 
	// http://bit.ly/2ig8gcH
    
    [CreateAssetMenu]
    public class Dialog : ScriptableObject {
        // @TODO: Change owner from string to NPC class
        public string owner;
        public string[] pages;
        private int activePage = 0;

        public void OnEnable ()
        {
            activePage = 0;
        }

        private void IncrementPage ()
        {
            activePage = Mathf.Clamp (activePage + 1, 0, pages.Length);
        }

        public string GetNextPage ()
        {
            string page = pages[activePage];
            IncrementPage ();
            return page;
        }

        public bool HasReachedEnd ()
        {
            return activePage == pages.Length;
        }
    }
}